# Els

## Usage

Create html elements with simple commands like:

new Els('h1').text('Bello world!').append('body');

&

var el = new Els('h2').html('<em>Hello Skiddo!</em>').append('div');

## TODO

Check whether the `new` keyword is necessary.