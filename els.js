(function(global) {
    var _els = function(element) {
        return new _els.fn.init(element);
    };
    _els.fn = _els.prototype = {
        then: function(thenValue) {
            this.thenValue[this.thenValue.length - 1] = thenValue;
            return this;
        },
        
        className: function() {
            this.element.className = Array.from(arguments).toString().replace(',', ' ');

            return this;
        },

        addClass: function(className) {
            this.element.classList.add(className);

            return this;
        },

        removeClass: function(className) {
            this.element.classList.remove(className);

            return this;
        },

        value: function(value) {
            this.element.value = value;

            return this;
        },

        setAttribute: function(attribute, value) {
            this.element.setAttribute(attribute, value);

            return this;
        },

        removeAttribute: function(attribute) {
            this.element.removeAttribute(attribute);

            return this;
        },

        text: function(text) {
            this.element.innerText = text;

            return this;
        },

        html: function(html) {
            this.element.innerHTML = html;

            return this;
        },

        child: function(child) {
            this.element.append(child.element);

            return this;
        },

        cloneChild: function(child) {
            this.element.append(child.element.cloneNode(true));

            return this;
        },

        append: function(target) {
            if(target === undefined) {
                target = document.querySelector('body');
            }
            else if(typeof(target) === 'string') {
                target = document.querySelector(target);
            }
            else if(target.element) {
                target = target.element;
            }

            target.append(this.element);

            return this;
        },

        remove: function() {
            this.element.remove();

            return this;
        },
    };

    var init = _els.fn.init = function(element) {
        if(typeof(element) === 'string') {
            this.element = document.createElement(element);
        }
        else if(element.element) {
            this.element = element.element.cloneNode(true);
        }
        return this;
    };
    init.prototype = _els.fn;

    global._els = _els;
})(this);