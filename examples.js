var el = _els('div'),
    el2 = _els('div');

el.text('Bello world!');
el.append();

el2.html('<strong>Hello Skiddo!</strong>').append('div');

var el3 = _els('h1').text('Bello world!').className('header', 'header--big').append(),
    el4 = _els('h2').html('<em>Hello Skiddo!</em>').append(),
    el5 = _els('h3').child(el).append(),
    el6 = _els('h4').cloneChild(el).append(),
    el7 = _els('hr').append(el2),
    el8 = _els('h1').text('Voorbeeld van tekst die live wordt geupdate').className('header', 'header--big').append(),
    el9 = _els('h1').text('I\'m Els!').setAttribute('data-foo', 'bar').setAttribute('data-bar', 'bar').removeAttribute('data-foo').setAttribute('data-bar', 'foo').append();

el8.removeClass('header--big').addClass('header--small');

window.setInterval(function() {
    el8.text('Geupdate tekst @' + new Date().getTime());
}, 1500);

el3.remove();

var clone = _els(el9).append(),
    input = _els('input').setAttribute('type', 'text').value(150).append();